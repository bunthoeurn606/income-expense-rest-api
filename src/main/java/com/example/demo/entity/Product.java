package com.example.demo.entity;

import com.example.demo.type.ProductType;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "product")
public class Product extends AuditableModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_name")
    private String productName;

    @Enumerated(value = EnumType.STRING)
    private ProductType type;

    private String category;

    private Long userId;

    private BigDecimal price;

}