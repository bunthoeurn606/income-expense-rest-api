package com.example.demo.entity;

import com.example.demo.type.PostgreSQLEnumType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@MappedSuperclass
@TypeDef(
		name = "pgsql_enum",
		typeClass = PostgreSQLEnumType.class
)
public abstract class AuditableModel implements Serializable {
	private static final long serialVersionUID = 7214313515451756580L;

	@CreatedDate
	@Column(name = "created_at")
	private Date createdDate;

	@LastModifiedDate
	@Column(name = "updated_at")
	private Date updatedDate;

}
