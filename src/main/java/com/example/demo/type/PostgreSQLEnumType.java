package com.example.demo.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.EnumType;

/**
 * @author Chuob Bunthoeurn
 */
@SuppressWarnings("rawtypes")
public class PostgreSQLEnumType extends EnumType {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7756858509553894885L;

	public void nullSafeSet(
            PreparedStatement st,
            Object value,
            int index,
            SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        if (value == null) {
            st.setNull(index, Types.OTHER);
        } else {
            st.setObject(
                    index,
                    value.toString(),
                    Types.OTHER
            );
        }
    }
}