package com.example.demo.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseCode {
    SUCCESS("200", "SUCCESS"),
    FAILED("400", "FAILED");

    final String code;
    final String description;
}
