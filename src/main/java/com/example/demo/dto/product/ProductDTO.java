package com.example.demo.dto.product;

import com.example.demo.type.ProductType;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDTO {
    private Long id;
    private String productName;
    private ProductType type;
    private String category;
    private BigDecimal price;
}
