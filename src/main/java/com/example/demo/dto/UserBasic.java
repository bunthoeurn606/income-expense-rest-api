package com.example.demo.dto;

import com.example.demo.entity.Users;
import org.springframework.security.core.userdetails.User;

/**
 * @author Chuob Bunthoeurn
 */
public class UserBasic extends User {
    private static final long serialVersionUID = 1L;

    public UserBasic(Users user) {
        super(user.getUsername(), user.getPassword(), user.getGrantedAuthoritiesList());
    }

    /*public UserBasic(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }*/
}
