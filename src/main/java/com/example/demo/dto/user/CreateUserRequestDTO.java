package com.example.demo.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateUserRequestDTO {
    private Long id;
    private String username;
    private String password;
}
