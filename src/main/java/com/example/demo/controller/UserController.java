package com.example.demo.controller;

import com.example.demo.dto.user.CreateUserRequestDTO;
import com.example.demo.mapper.UserMapper;
import com.example.demo.services.UserService;
import com.example.demo.vo.ResponseMessageBuilder;
import com.example.demo.vo.user.CreateUserRequestVO;
import com.example.demo.vo.user.UserResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseMessageBuilder.ResponseMessage<Void> post(@Valid @RequestBody CreateUserRequestVO requestVO) throws Exception {
        CreateUserRequestDTO requestDTO = UserMapper.INSTANCE.from(requestVO);
        userService.create(requestDTO);
        return new ResponseMessageBuilder<Void>().success().build();
    }


    @GetMapping
    public ResponseMessageBuilder.ResponseMessage<List<UserResponseVO>> get() {
        return new ResponseMessageBuilder<List<UserResponseVO>>().success().addData(UserMapper.INSTANCE.fromDTO(userService.list())).build();
    }
}
