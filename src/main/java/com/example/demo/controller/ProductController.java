package com.example.demo.controller;

import com.example.demo.dto.product.ProductDTO;
import com.example.demo.dto.user.CreateUserRequestDTO;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.services.ProductService;
import com.example.demo.services.UserService;
import com.example.demo.vo.ResponseMessageBuilder;
import com.example.demo.vo.product.IncomeExpenseResponseVO;
import com.example.demo.vo.product.ProductCreateRequestVO;
import com.example.demo.vo.product.ProductResponseVO;
import com.example.demo.vo.user.CreateUserRequestVO;
import com.example.demo.vo.user.UserResponseVO;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseMessageBuilder.ResponseMessage<Void> post(@Valid @RequestBody ProductCreateRequestVO requestVO) {
        ProductDTO requestDTO = ProductMapper.INSTANCE.from(requestVO);
        productService.create(requestDTO);
        return new ResponseMessageBuilder<Void>().success().build();
    }


    @GetMapping
    public ResponseMessageBuilder.ResponseMessage<List<ProductResponseVO>> get() {
        return new ResponseMessageBuilder<List<ProductResponseVO>>().success().addData(ProductMapper.INSTANCE.from(productService.list())).build();
    }

    @GetMapping("/getExpense")
    public ResponseMessageBuilder.ResponseMessage<IncomeExpenseResponseVO> getExpense() {
        return new ResponseMessageBuilder<IncomeExpenseResponseVO>().success().addData(ProductMapper.INSTANCE.from(productService.getExpense())).build();
    }
}
