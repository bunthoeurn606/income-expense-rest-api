package com.example.demo.repository;

import com.example.demo.entity.Product;
import com.example.demo.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * @author Chuob Bunthoeurn
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "select sum(p.price) from product p where p.type = 'EXPENSE'", nativeQuery = true)
    BigDecimal sumExpense();

    @Query(value = "select sum(p.price) from product p where p.type = 'INCOME'", nativeQuery = true)
    BigDecimal sumIncome();
}
