package com.example.demo.mapper;

import com.example.demo.dto.user.CreateUserRequestDTO;
import com.example.demo.dto.user.UserResponseDTO;
import com.example.demo.entity.Users;
import com.example.demo.vo.user.CreateUserRequestVO;
import com.example.demo.vo.user.UserResponseVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    Users from(CreateUserRequestDTO userRequestDTO);
    CreateUserRequestDTO from(CreateUserRequestVO userRequestVO);
    List<UserResponseVO> fromDTO(List<UserResponseDTO> list);
    List<UserResponseDTO> from(List<Users> users);
    UserResponseVO map(UserResponseDTO value);
    UserResponseDTO map(Users value);
}
