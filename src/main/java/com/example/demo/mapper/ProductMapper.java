package com.example.demo.mapper;

import com.example.demo.dto.product.IncomeExpenseResponseDTO;
import com.example.demo.dto.product.ProductDTO;
import com.example.demo.entity.Product;
import com.example.demo.vo.product.IncomeExpenseResponseVO;
import com.example.demo.vo.product.ProductCreateRequestVO;
import com.example.demo.vo.product.ProductResponseVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    List<ProductDTO> fromModel (List<Product> list);
    List<ProductResponseVO> from (List<ProductDTO> list);
    ProductResponseVO fromDTO(ProductDTO productResponseDTO);
    ProductDTO from (ProductCreateRequestVO productCreateRequestVO);
    Product from (ProductDTO productDTO);
    IncomeExpenseResponseVO from (IncomeExpenseResponseDTO productDTO);

}
