package com.example.demo.services;

import com.example.demo.dto.product.IncomeExpenseResponseDTO;
import com.example.demo.dto.product.ProductDTO;

import java.util.List;

public interface ProductService {
    void create(ProductDTO productDTO);
    List<ProductDTO> list();
    IncomeExpenseResponseDTO getExpense();
}
