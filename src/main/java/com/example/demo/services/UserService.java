package com.example.demo.services;

import com.example.demo.dto.user.CreateUserRequestDTO;
import com.example.demo.dto.user.UserResponseDTO;
import com.example.demo.vo.user.CreateUserRequestVO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @author Chuob Bunthoeurn
 */
public interface UserService extends UserDetailsService {
    void create(CreateUserRequestDTO requestDTO) throws Exception;
    List<UserResponseDTO> list();
}
