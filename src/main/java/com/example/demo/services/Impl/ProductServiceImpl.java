package com.example.demo.services.Impl;

import com.example.demo.dto.product.IncomeExpenseResponseDTO;
import com.example.demo.dto.product.ProductDTO;
import com.example.demo.entity.Product;
import com.example.demo.entity.Users;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public void create(ProductDTO productDTO) {
        Product product = ProductMapper.INSTANCE.from(productDTO);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users usersEx = new Users();
        Long userId = null;
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            usersEx.setUsername(username);
        } else {
            String username = principal.toString();
            usersEx.setUsername(username);
        }
        Users users = userRepository.findOne(Example.of(usersEx)).orElse(null);
        if (users != null) userId = users.getId();
        product.setUserId(userId);
        productRepository.save(product);
    }

    @Override
    public List<ProductDTO> list() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users usersEx = new Users();
        Long userId = null;
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            usersEx.setUsername(username);
        } else {
            String username = principal.toString();
            usersEx.setUsername(username);
        }
        Users users = userRepository.findOne(Example.of(usersEx)).orElse(null);
        if (users != null) userId = users.getId();
        Product productEx = new Product();
        productEx.setUserId(userId);
        return ProductMapper.INSTANCE.fromModel(productRepository.findAll(Example.of(productEx)));
    }

    @Override
    public IncomeExpenseResponseDTO getExpense() {
        IncomeExpenseResponseDTO responseDTO = new IncomeExpenseResponseDTO();
        responseDTO.setExpense(productRepository.sumExpense());
        responseDTO.setIncome(productRepository.sumIncome());
        return responseDTO;
    }
}
