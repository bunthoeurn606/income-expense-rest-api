package com.example.demo.services.Impl;

import com.example.demo.dto.user.CreateUserRequestDTO;
import com.example.demo.dto.user.UserResponseDTO;
import com.example.demo.mapper.UserMapper;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.UserService;
import com.example.demo.dto.UserBasic;
import com.example.demo.entity.Users;
import com.example.demo.repository.OauthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @author Chuob Bunthoeurn
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    OauthRepository oauthDao;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserBasic loadUserByUsername(final String username) throws UsernameNotFoundException {
        Users userEntity;
        try {
            userEntity = oauthDao.getUserDetails(username);
            return new UserBasic(userEntity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
    }

    @Override
    @Transactional
    public void create(CreateUserRequestDTO requestDTO) throws Exception {
        Users users = UserMapper.INSTANCE.from(requestDTO);
        String username = users.getUsername();
        Users usersEx = new Users();
        usersEx.setUsername(username);
        Users userModel = userRepository.findOne(Example.of(usersEx)).orElse(null);
        if (!Objects.isNull(userModel))
            throw new Exception("Username already exist");

        userRepository.save(users);
    }

    @Override
    public List<UserResponseDTO> list() {
        return UserMapper.INSTANCE.from(userRepository.findAll());
    }
}
