package com.example.demo.vo.product;

import com.example.demo.type.ProductType;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductCreateRequestVO {
    private String productName;
    private ProductType type;
    private String category;
    private BigDecimal price;
}
