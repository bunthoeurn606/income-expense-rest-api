package com.example.demo.vo;

import com.example.demo.type.ResponseCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ResponseMessageBuilder<T> {

    private final ResponseMessage<T> message = new ResponseMessage<T>();

    public ResponseMessageBuilder<T> success() {
        this.message.result = Boolean.TRUE;
        this.message.resultCode = ResponseCode.SUCCESS.getCode();
        this.message.resultMessage = ResponseCode.SUCCESS.getDescription();
        this.message.status = HttpStatus.OK;
        return this;
    }

    public ResponseMessageBuilder<T> fail(final String code, final String message) {
        this.message.result = Boolean.FALSE;
        this.message.resultCode = code;
        this.message.resultMessage = message;
        this.message.status = HttpStatus.BAD_REQUEST;
        return this;
    }

    /**
     * Add Error Code
     *
     * @param code
     * @param message
     * @return
     */
    public ResponseMessageBuilder<T> addCode(final String code, final String message) {
        this.message.setResultCode(code);
        this.message.setResultMessage(message);
        return this;
    }

    /**
     * Add Response Data
     *
     * @param data
     * @return
     */
    public ResponseMessageBuilder<T> addData(final T data) {
        this.message.setBody(data);
        return this;
    }

    /**
     * Finally call to construct ResponseMessage
     *
     * @return
     */
    public final ResponseMessage<T> build() {
        return message;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ResponseMessage<T> implements Serializable, Cloneable {

        private static final long serialVersionUID = 127922628199902941L;

        private Boolean result = false;
        private String resultCode;
        private String resultMessage;
        private String traceId;
        private T body;
        private String error;
        @JsonIgnore
        private HttpStatus status;

        public ResponseMessage() {
        }

        public ResponseMessage<T> body(T body) {
            this.body = body;
            return this;
        }

        public ResponseMessage<T> status(HttpStatus status) {
            this.status = status;
            return this;
        }

        public ResponseMessage<T> error(String error) {
            this.error = error;
            return this;
        }

        public ResponseMessage<T> traceId(String traceId) {
            this.traceId = traceId;
            return this;
        }

        public static <T> ResponseMessage<T> success() {
            return success(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDescription());
        }

        public static <T> ResponseMessage<T> success(String code, String message) {
            return build(code, message, true, HttpStatus.OK);
        }

        public static <T> ResponseMessage<T> success(String code, String message, T body) {
            ResponseMessage<T> _this = build(code, message, true, HttpStatus.OK);
            _this.body = body;
            return _this;
        }

        public static <T> ResponseMessage<T> fail(String code, String message) {
            return build(code, message, false, HttpStatus.BAD_REQUEST);
        }

        public static <T> ResponseMessage<T> build(String code, String message, boolean result, HttpStatus status) {
            return build(code, message, result, status, null);
        }

        public static <T> ResponseMessage<T> build(String code, String message, boolean result, HttpStatus status, T body) {
            ResponseMessage<T> _this = new ResponseMessage<T>();
            _this.result = result;
            _this.resultCode = code;
            _this.resultMessage = message;
            _this.status = status;
            _this.body = body;
            return _this;
        }
    }
}