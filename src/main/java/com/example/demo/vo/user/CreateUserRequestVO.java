package com.example.demo.vo.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateUserRequestVO {

    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
