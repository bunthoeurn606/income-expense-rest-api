package com.example.demo.vo.user;

import lombok.Data;

@Data
public class UserResponseVO{
    private Long id;
    private String username;
}
