package com.example.demo.servlet;

import org.springframework.security.web.savedrequest.Enumerator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/* https://stackoverflow.com/questions/38165131/spring-security-oauth2-accept-json/38284206 */
public class OAuthRequestWrapper extends HttpServletRequestWrapper {

	private final HashMap<String, String[]> params;

	public OAuthRequestWrapper(HttpServletRequest request, HashMap<String, String[]> params) {
		super(request);
		this.params = params;
	}

	@Override
	public String getParameter(String name) {
		if (this.params.containsKey(name)) {
			return this.params.get(name)[0];
		}
		return "";
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		return this.params;
	}

	@Override
	public Enumeration<String> getParameterNames() {
		return new Enumerator<>(params.keySet());
	}

	@Override
	public String[] getParameterValues(String name) {
		return params.get(name);
	}

}
