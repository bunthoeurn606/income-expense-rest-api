create table if not exists users (
    id bigserial,
    username varchar (255),
    password varchar (255),
    created_at timestamp default now(),
    updated_at timestamp,
    primary key (id)
);


create table if not exists product (
     id bigserial,
     "type" varchar (255),
     user_id bigint,
     category varchar (255),
     product_name varchar (255),
     price decimal,
     created_at timestamp default now(),
     updated_at timestamp,
     primary key (id)
);